import java.io.File

fun main(args: Array<String>) {
  val rules = mutableMapOf<String, StringRule>()
  val expressions = mutableListOf<String>()

  File("input/day19-test.txt").forEachLine {
    if (it.isNotBlank()) {
      if (it.contains(':')) {
        val pieces = it.split(": ")
        rules[pieces[0]] = StringRule(pieces[1].split(" | ").toList())
      } else {
        expressions.add(it.trim())
      }
    }
  }

  println(rules)
  println(expressions)
}

fun evaluateAll(rules: MutableMap<String, StringRule>): MutableSet<String> {
  val all = mutableSetOf<String>()
  val current = mutableListOf<List<StringRule>>()
  val next = mutableListOf<List<StringRule>>()
  current.add(mutableListOf(rules["0"]!!))

  while (current.isNotEmpty()) {
    val i = current.iterator()

    while (i.hasNext()) {
      val current = i.next()
      if (current.all { it.isTerminator() }) all.add(current.joinToString { it.terminator() })
      next.addAll(evaluateFirst(rules, current))

    }
  }

  return all
}

fun evaluateFirst(rules: MutableMap<String, StringRule>, rule: List<StringRule>): List<List<StringRule>> {
  val newRules = mutableListOf<List<StringRule>>()

  for (i in rule.indices) {
    if (!rule[i].isTerminator()) {

    }
  }

  return newRules
}

data class StringRule(val children: List<String>) {
  fun isTerminator() = children.size == 1 && children[0][0] == '"'
  fun terminator() = children[0].replace("\"", "")
}

