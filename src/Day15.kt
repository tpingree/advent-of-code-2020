fun main(args: Array<String>) {
  var last = 7
  val numberHistory = mutableMapOf(12 to 1, 20 to 2, 0 to 3, 6 to 4, 1 to 5, 17 to 6)
  var turn = 7

  do {
    val lastTurnSpoke = numberHistory.getOrElse(last) { -1 }
    numberHistory.put(last, turn)
    last = if (lastTurnSpoke == -1) 0 else turn - lastTurnSpoke
    turn++
  } while (turn != 30000000)

  println(last)
}



