import java.io.File

fun main(args: Array<String>) {
  // day7part1()
  day7part2()
}

fun day7part1() {
  val rules = day7input()
  val visitedColors = mutableSetOf<String>()
  val visitNext = mutableListOf<String>()
  val currentColors = mutableListOf<String>()

  visitNext.add("shiny gold")

  while (visitNext.isNotEmpty()) {
    currentColors.clear()
    currentColors.addAll(visitNext)
    visitNext.clear()

    for (color in currentColors) {
      val checkThese = rules.filter { rule -> rule.value.any { it.color == color } }.map { it.key }

      for (check in checkThese) {
        if (!visitedColors.contains(check)) {
          visitedColors.add(check)
          visitNext.add(check)
        }
      }
    }
  }

  println(visitedColors.size)
}

fun day7part2() {
  val rules = day7input()
  var totalBags = 0

  var currentBags = mutableListOf<BagContents>();
  currentBags.addAll(rules["shiny gold"]!!.map { it.copy() })

  while (currentBags.isNotEmpty()) {
    val currentBag = currentBags.removeAt(0)
    var currentBagRules = rules[currentBag.color]!!

    totalBags += currentBag.count
    currentBags.addAll(currentBagRules.map {
      BagContents(it.count * currentBag.count, it.color)
    })
  }

  println(totalBags)
}

fun day7input(): MutableMap<String, List<BagContents>> {
  val rules = mutableMapOf<String, List<BagContents>>()

  File("input/day7.txt").forEachLine {
    val line = it.trim().replace(".", "")
    var pieces = line.split(" bags contain ")

    var color = pieces[0]

    var contains = listOf<BagContents>()
    if (pieces[1] != "no other bags") {
      contains = pieces[1]
        .replace(Regex(" bags?"), "")
        .split(", ")
        .map {
          var pieces = it.split(" ", ignoreCase = true, limit = 2)
          BagContents(pieces[0].toInt(), pieces[1])
        }
    }

    rules[color] = contains
  }

  return rules
}

data class BagContents(val count: Int, val color: String)
