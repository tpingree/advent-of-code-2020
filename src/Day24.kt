import java.io.File

fun main(args: Array<String>) {
  val blackTiles = mutableSetOf<HexTile>()

  File("input/day24.txt").forEachLine {
    var x = 0
    var y = 0
    val directions = Regex("[ns]?[ew]").findAll(it.trim())

    for (direction in directions) {
      when (direction.value) {
        "e" -> x += 1
        "w" -> x -= 1
        "ne" -> y += 1
        "nw" -> {
          x -= 1
          y += 1
        }
        "se" -> {
          x += 1
          y -= 1
        }
        "sw" -> y -= 1
      }
    }

    val tile = HexTile(x, y)

    if (blackTiles.contains(tile)) {
      blackTiles.remove(tile)
    } else {
      blackTiles.add(tile)
    }
  }

  println(blackTiles.size)

  val newBlackTiles = mutableSetOf<HexTile>()
  val visited = mutableSetOf<HexTile>()
  for (i in 1..100) {
    newBlackTiles.clear()
    visited.clear()

    for (tile in blackTiles) {
      val neighbors = neighbors(tile)
      visited.add(tile)

      if (blackTiles.intersect(neighbors).size in 1..2) newBlackTiles.add(tile)

      for (neighbor in neighbors) {
        if (visited.contains(neighbor)) continue
        visited.add(neighbor)
        if (blackTiles.intersect(neighbors(neighbor)).size == 2) newBlackTiles.add(neighbor)
      }
    }

    blackTiles.clear()
    blackTiles.addAll(newBlackTiles)
    println("Day ${i}: ${blackTiles.size}")
  }
}

fun neighbors(tile: HexTile): List<HexTile> {
  val neighbors = mutableListOf<HexTile>()
  neighbors.add(HexTile(tile.x - 1, tile.y))
  neighbors.add(HexTile(tile.x + 1, tile.y))
  neighbors.add(HexTile(tile.x, tile.y + 1))
  neighbors.add(HexTile(tile.x, tile.y - 1))
  neighbors.add(HexTile(tile.x + 1, tile.y - 1))
  neighbors.add(HexTile(tile.x - 1, tile.y + 1))
  return neighbors
}

data class HexTile(val x: Int, val y: Int)