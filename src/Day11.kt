import java.io.File

fun main(args: Array<String>) {
  // day11part1()
  day11part2()
}

fun day11part1() {
  var current = day11input()
  var next = day11input()

  do {
    current.next(next)
    current = next.also { next = current }
  } while (!current.equals(next))
  println(next.countOccupied())
}

fun day11part2() {
  var current = day11input()
  var next = day11input()

  do {
    current.nextWithSight(next)
    current = next.also { next = current }
  } while (!current.equals(next))
  println(next)
  println(next.countOccupied())
}


fun day11input(): Seating {
  val input = mutableListOf<CharArray>()
  File("input/day11.txt").forEachLine {
    input.add(it.trim().toCharArray())
  }
  return Seating(input)
}

class Seating(val layout: List<CharArray>) {
  fun equals(check: Seating): Boolean {
    for (i in check.layout.indices) {
      if (!layout[i].contentEquals(check.layout[i])) return false
    }

    return true
  }

  fun next(dest: Seating) {
    for (i in layout.indices) {
      for (j in layout[i].indices) {
        dest.layout[i][j] = when (layout[i][j]) {
          'L' -> if (count(true, i, j) == 0) '#' else 'L'
          '#' -> if (count(true, i, j) >= 4) 'L' else '#'
          else -> '.'
        }
      }
    }
  }


  fun nextWithSight(dest: Seating) {
    for (i in layout.indices) {
      for (j in layout[i].indices) {
        dest.layout[i][j] = when (layout[i][j]) {
          'L' -> if (countWithSight(true, i, j) == 0) '#' else 'L'
          '#' -> if (countWithSight(true, i, j) >= 5) 'L' else '#'
          else -> '.'
        }
      }
    }
  }

  private fun count(occupied: Boolean, i: Int, j: Int): Int {
    var count = seatOccupied(i - 1, j - 1) +
      seatOccupied(i, j - 1) +
      seatOccupied(i + 1, j - 1) +
      seatOccupied(i - 1, j) +
      seatOccupied(i + 1, j) +
      seatOccupied(i - 1, j + 1) +
      seatOccupied(i, j + 1) +
      seatOccupied(i + 1, j + 1)

    return if (occupied) count else 8 - count
  }

  private fun countWithSight(occupied: Boolean, i: Int, j: Int): Int {
    var count = 0

    for (dy in -1..1) {
      for (dx in -1..1) {
        if (!(dy == 0 && dx == 0)) {
          count += seatOccupiedV2(i, j, dx, dy)
        }
      }
    }

    return if (occupied) count else 8 - count
  }

  private fun seatOccupied(i: Int, j: Int): Int {
    if (i < 0 || i >= layout.size) return 0
    if (j < 0 || j >= layout[0].size) return 0
    return if (layout[i][j] == '#') 1 else 0
  }

  private fun seatOccupiedV2(startI: Int, startJ: Int, dx: Int, dy: Int): Int {
    var i = startI
    var j = startJ

    while (true) {
      i += dx
      j += dy

      if (i < 0 || i >= layout.size) return 0
      if (j < 0 || j >= layout[0].size) return 0
      if (layout[i][j] == 'L') return 0
      if (layout[i][j] == '#') return 1
    }
  }

  fun countOccupied(): Int {
    var count = 0

    for (i in layout.indices) {
      for (j in layout[i].indices) {
        count += if (layout[i][j] == '#') 1 else 0
      }
    }

    return count
  }

  override fun toString(): String {
    val builder = StringBuilder()

    for (i in layout.indices) {
      for (j in layout[i].indices) {
        builder.append(layout[i][j])
      }
      builder.append('\n')
    }

    return builder.toString()
  }
}