import java.math.BigInteger

fun main(args: Array<String>) {
  var pkey1 = BigInteger.valueOf(9789649)
  var pkey2 = BigInteger.valueOf(3647239)

  var loop1 = loopSize(pkey1)
  var loop2 = loopSize(pkey2)

  println(transform(pkey1, loop2))
  println(transform(pkey2, loop1))
}

fun loopSize(pkey: BigInteger): BigInteger {
  var subject = BigInteger.valueOf(7)
  var special = BigInteger.valueOf(20201227)
  var value = BigInteger.ONE

  var loops = BigInteger.ZERO

  while (value != pkey) {
    value = value.multiply(subject)
    value = value.mod(special)
    loops = loops.inc()
  }

  return loops
}

fun transform(subject: BigInteger, initialLoops: BigInteger): BigInteger {
  var value = BigInteger.ONE
  var loops = initialLoops
  var special = BigInteger.valueOf(20201227)

  while (loops.compareTo(BigInteger.ZERO) > 0) {
    loops = loops.dec()
    value = value.multiply(subject)
    value = value.mod(special)
  }

  return value
}