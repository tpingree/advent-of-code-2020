import java.io.File
import java.math.BigInteger

fun main(args: Array<String>) {
  val pieces = File("input/day16.txt").readText(Charsets.UTF_8).trim().split("\n\n")

  val rules = pieces[0].split("\n").map {
    Rule(it.split(": ")[0], it.split(": ")[1].split(" or ").map {
      val raw = it.split("-")
      Range(raw[0].toInt(), raw[1].toInt())
    })
  }

  val yours = Ticket(pieces[1].replace("your ticket:\n", "").split(",").map { it.toInt() })

  val nearby = pieces[2].replace("nearby tickets:\n", "").split("\n").map {
    Ticket(it.split(",").map { it.toInt() })
  }

  day16part1(rules, nearby)
  day16part2(rules, yours, nearby)
}

fun day16part1(rules: List<Rule>, nearby: List<Ticket>) {
  var ticketScanningErrorRate = 0
  for (ticket in nearby) {
    for (value in ticket.values) {
      if (!rules.all { it.passes(value) }) {
        ticketScanningErrorRate += value
      }
    }
  }

  println(ticketScanningErrorRate)
}

fun day16part2(rules: List<Rule>, yours: Ticket, nearby: List<Ticket>) {
  var valid = nearby.filter {
    it.values.all {
      rules.any { rule -> rule.passes(it) }
    }
  }

  println("${valid.size}/${nearby.size} of ticket are valid")

  for (rule in rules) {
    for (i in valid[0].values.indices) {
      if (valid.all { rule.passes(it.values[i]) }) {
        println("Rule \"${rule.description}\" works for every value in column $i.")
      }
    }
  }

  /*
  o Rule "arrival platform" works for every value in column 0.
  o Rule "train" works for every value in column 1.
  --- Rule "departure platform" works for every value in column 2.
  o Rule "duration" works for every value in column 3.
  --- Rule "departure track" works for every value in column 4.
  o Rule "price" works for every value in column 5.
  o Rule "arrival station" works for every value in column 6.
  --- Rule "departure date" works for every value in column 7.
  o Rule "type" works for every value in column 8.
  o Rule "wagon" works for every value in column 9.
  o Rule "seat" works for every value in column 10.
  --- Rule "departure location" works for every value in column 11.
  o Rule "class" works for every value in column 12.
  o Rule "row" works for every value in column 13.
  --- Rule "departure station" works for every value in column 14.
  o Rule "arrival track" works for every value in column 15.
  --- Rule "departure time" works for every value in column 16.
  o Rule "route" works for every value in column 17.
  o Rule "arrival location" works for every value in column 18.
  o Rule "zone" works for every value in column 19.
  
  Process finished with exit code 0
  */

  println(
    BigInteger.valueOf(yours.values[7].toLong())
      .multiply(BigInteger.valueOf(yours.values[16].toLong()))
      .multiply(BigInteger.valueOf(yours.values[11].toLong()))
      .multiply(BigInteger.valueOf(yours.values[2].toLong()))
      .multiply(BigInteger.valueOf(yours.values[4].toLong()))
      .multiply(BigInteger.valueOf(yours.values[14].toLong()))
  )
}

data class Rule(val description: String, val ranges: List<Range>) {
  fun passes(value: Int) = ranges.any { it.passes(value) }
}

data class Range(val start: Int, val end: Int) {
  fun passes(value: Int) = value in start..end
}

data class Ticket(val values: List<Int>)