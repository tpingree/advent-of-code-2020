import java.io.File
import java.math.BigInteger

fun main(args: Array<String>) {
  var rawTiles = File("input/day20.txt").readText().trim().split("\n\n")
  var tiles = mutableListOf<Tile>()

  for (rawTile in rawTiles) {
    val pieces = rawTile.split("\n").toMutableList()
    val id = pieces[0].replace("Tile ", "").replace(":", "").toBigInteger()
    pieces.removeAt(0)
    val image = pieces.joinToString("")

    tiles.add(Tile(id, image))
  }

  var matchCounts = mutableMapOf<BigInteger, Int>()
  for (i in 0 until tiles.size) {
    for (j in (i + 1) until tiles.size) {
      val matches = tiles[i].sides().intersect(tiles[j].sides()).size / 2
      matchCounts.putIfAbsent(tiles[i].id, 0)
      matchCounts[tiles[i].id] = matchCounts[tiles[i].id]!! + matches
      matchCounts.putIfAbsent(tiles[j].id, 0)
      matchCounts[tiles[j].id] = matchCounts[tiles[j].id]!! + matches

      println("Tile ${tiles[i].id} matched ${tiles[j].id} -> ${tiles[i].sides().intersect(tiles[j].sides()).size} time(s)")
    }
  }

  println(matchCounts)
  println(matchCounts.filter { it.value == 2 }.keys.fold(BigInteger.ONE, BigInteger::multiply))
}

data class Tile(val id: BigInteger, val image: String) {
  fun sides(): MutableSet<String> {
    val sides = mutableSetOf<String>()
    sides.add(image.substring(0..9))
    sides.add(image.substring(0..9).reversed())
    sides.add(image.substring((9 * 10)..(9 * 10 + 9)))
    sides.add(image.substring((9 * 10)..(9 * 10 + 9)).reversed())

    val left = StringBuilder()
    val right = StringBuilder()
    for (i in 0 until 10) {
      left.append(image[i * 10])
      right.append(image[i * 10 + 9])
    }

    sides.add(left.toString())
    sides.add(left.toString().reversed())
    sides.add(right.toString())
    sides.add(right.toString().reversed())

    return sides
  }
}