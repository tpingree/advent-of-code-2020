import java.io.File
import kotlin.streams.toList

fun main(args: Array<String>) {
  day3part1()
  day3part2()
}

fun day3part1() {
  println(countTrees(3, 1))
}

fun day3part2() {
  var a = countTrees(1, 1)
  var b = countTrees(3, 1)
  var c = countTrees(5, 1)
  var d = countTrees(7, 1)
  var e = countTrees(1, 2)

  var answer = a.toBigDecimal()
    .multiply(b.toBigDecimal())
    .multiply(c.toBigDecimal())
    .multiply(d.toBigDecimal())
    .multiply(e.toBigDecimal())

  println("$a * $b * $c * $d * $e = $answer")
}

fun countTrees(dx: Int, dy: Int): Int {
  var terrain = convertDay3Input().filterIndexed { i, _ -> i % dy == 0 }

  var x = 0
  return terrain.map {
    val tree = it.isTree(x)
    x += dx
    tree
  }.filter { it }.count()
}

fun convertDay3Input(): List<Terrain> {
  var terrains = mutableListOf<Terrain>()

  File("input/day3.txt").forEachLine {
    terrains.add(Terrain(
      it.chars()
        .mapToObj { c -> c == '#'.toInt() }
        .toList()
    ));
  }

  return terrains
}

class Terrain(var trees: List<Boolean>) {
  fun isTree(x: Int) = trees[x % trees.size]
}