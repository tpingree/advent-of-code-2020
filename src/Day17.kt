import java.io.File


fun day17input(): MutableSet<Cube> {
  var cubes = mutableSetOf<Cube>()

  var y = 0
  File("input/day17.txt").forEachLine {
    for (x in it.trim().indices) {
      if (it[x] == '#') {
        cubes.add(Cube(x, y, 0, 0))
      }
    }

    y++
  }

  return cubes
}

fun main(args: Array<String>) {
  val cubes = day17input()

  val next = mutableSetOf<Cube>()
  val visited = mutableSetOf<Cube>()

  for (i in 1..6) {
    for (cube in cubes) {
      if (countActive(cube, cubes) in 2..3) {
        next.add(cube)
      }

      for (dx in -1..1) {
        for (dy in -1..1) {
          for (dz in -1..1) {
            for (dw in -1..1) {
              var check = Cube(cube.x + dx, cube.y + dy, cube.z + dz, cube.w + dw)
              if ((dx == 0 && dy == 0 && dz == 0 && dw == 0) || cubes.contains(check) || visited.contains(check)) {
                continue
              }

              visited.add(check)
              if (countActive(check, cubes) == 3) {
                next.add(check)
              }
            }
          }
        }
      }
    }

    cubes.clear()
    cubes.addAll(next)
    next.clear()
    visited.clear()
  }

  println(cubes.size)
}

data class Cube(val x: Int, val y: Int, val z: Int, val w: Int)

fun countActive(cube: Cube, cubes: MutableSet<Cube>): Int {
  return countActive(cube.x, cube.y, cube.z, cube.w, cubes)
}

fun countActive(x: Int, y: Int, z: Int, w: Int, cubes: MutableSet<Cube>): Int {
  var active = 0

  for (dx in -1..1) {
    for (dy in -1..1) {
      for (dz in -1..1) {
        for (dw in -1..1) {
          if (dx == 0 && dy == 0 && dz == 0 && dw == 0) {
            continue
          }

          active += if (cubes.contains(Cube(x + dx, y + dy, z + dz, w + dw))) 1 else 0
        }
      }
    }
  }

  return active
}

