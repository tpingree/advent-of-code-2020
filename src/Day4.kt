import java.io.File

fun main(args: Array<String>) {
  day4part1()
  day4part2()
}

fun day4part1() {
  println(convertDay4Input().count { it.missingOnlyCountry() })
}

fun day4part2() {
  println(convertDay4Input().count { it.valid() })
}

fun convertDay4Input(): List<Passport> {
  var file = File("input/day4.txt").readText()

  return file.trim().split("\n\n").map {
    val tags = mutableMapOf<String, String>()

    it.split("\n", " ").forEach {
      val pieces = it.split(":");
      tags[pieces[0]] = pieces[1]
    }

    Passport(tags)
  }.toList();
}


data class Passport(val tags: Map<String, String>) {
  fun missingOnlyCountry() = tags.size == 8 || (tags.size == 7 && !tags.containsKey("cid"))
  fun valid(): Boolean {
    if (!missingOnlyCountry()) return false

    if (!numberValidate(tags["byr"]!!, 1920, 2002)) return false
    if (!numberValidate(tags["iyr"]!!, 2010, 2020)) return false
    if (!numberValidate(tags["eyr"]!!, 2020, 2030)) return false

    if (tags["hgt"]!!.contains("cm")) {
      if (!regexValidate(tags["hgt"]!!, "[0-9]{3}cm")) return false
      if (!numberValidate(tags["hgt"]!!.replace("cm", ""), 150, 193)) return false
    } else {
      if (!regexValidate(tags["hgt"]!!, "[0-9]{2}in")) return false
      if (!numberValidate(tags["hgt"]!!.replace("in", ""), 59, 76)) return false
    }

    if (!regexValidate(tags["hcl"]!!, "#[0-9a-f]{6}")) return false
    if (!regexValidate(tags["ecl"]!!, "(amb|blu|brn|gry|grn|hzl|oth)")) return false
    if (!regexValidate(tags["pid"]!!, "[0-9]{9}")) return false
    return true
  }
}

fun numberValidate(field: String, min: Int, max: Int): Boolean {
  return field.toInt() in min..max
}

fun regexValidate(field: String, regex: String): Boolean {
  return field.matches(Regex(regex))
}
