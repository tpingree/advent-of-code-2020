import java.io.File
import java.math.BigInteger

fun main(args: Array<String>) {
  day10part2()
}

fun day10part2() {
  val raw = day10input()
  var current = 0
  val input = mutableListOf<Int>()

  for (i in raw) {
    input.add(i - current)
    current = i
  }
  input.add(3)

  var answer = BigInteger.ONE
  var listLength = 0
  for (i in input) {
    listLength++

    if (i == 3) {
      val paths = when (listLength) {
        0, 1, 2 -> 1
        3 -> 2
        4 -> 4
        else -> ((8 * (listLength - 4)) - 1)
      }

      answer = answer.multiply(BigInteger.valueOf(paths.coerceAtLeast(1).toLong()))
      listLength = 0
    }
  }
  println(answer)
}

fun day10input(): List<Int> {
  val input = mutableListOf<Int>()
  File("input/day10.txt").forEachLine {
    input.add(it.trim().toInt())
  }
  input.sort()
  return input
}