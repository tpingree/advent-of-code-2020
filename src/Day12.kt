import java.io.File

fun main(args: Array<String>) {
  day12part1()
  day12part2()
}

fun day12part1() {
  val instructions = day12input()
  val ferry = Ferry()
  for (instruction in instructions) ferry.move(instruction)
  println("Part 1: (${ferry.x}, ${ferry.y})")
}


fun day12part2() {
  val instructions = day12input()
  val ferry = Ferry()
  for (instruction in instructions) ferry.moveWithWaypoint(instruction)
  println("Part 2: (${ferry.x}, ${ferry.y})")
}


class Ferry() {
  var x = 0
  var y = 0
  var direction = FerryDirection.EAST
  var waypoint = Waypoint(10, 1)
  val directions = mapOf(
    'N' to FerryDirection.NORTH,
    'E' to FerryDirection.EAST,
    'S' to FerryDirection.SOUTH,
    'W' to FerryDirection.WEST,
  )

  fun move(instruction: FerryInstruction) {
    when (instruction.op) {
      'F' -> {
        this.x += direction.dx * instruction.amount
        this.y += direction.dy * instruction.amount
      }
      'N', 'E', 'S', 'W' -> {
        this.x += directions[instruction.op]!!.dx * instruction.amount
        this.y += directions[instruction.op]!!.dy * instruction.amount
      }
      'L', 'R' -> {
        val index = ((direction.index + ((if (instruction.op == 'L') -1 else 1) * (instruction.amount / 90))) + 4) % 4
        this.direction = FerryDirection.values().findLast { it.index == index }!!
      }
    }
  }

  fun moveWithWaypoint(instruction: FerryInstruction) {
    when (instruction.op) {
      'F' -> {
        this.x += waypoint.x * instruction.amount
        this.y += waypoint.y * instruction.amount
      }
      'N', 'E', 'S', 'W' -> {
        waypoint.x += directions[instruction.op]!!.dx * instruction.amount
        waypoint.y += directions[instruction.op]!!.dy * instruction.amount
      }
      'L', 'R' -> {
        var times = instruction.amount / 90
        for (i in 0 until times) {
          waypoint.x = waypoint.y.also { waypoint.y = waypoint.x }
          waypoint.x *= if (instruction.op == 'L') -1 else 1
          waypoint.y *= if (instruction.op == 'R') -1 else 1
        }
      }
    }
  }
}

data class Waypoint(var x: Int, var y: Int)
data class FerryInstruction(val op: Char, val amount: Int)

fun day12input(): MutableList<FerryInstruction> {
  val instructions = mutableListOf<FerryInstruction>()
  File("input/day12.txt").forEachLine {
    instructions.add(FerryInstruction(it[0], it.drop(1).trim().toInt()))
  }
  return instructions
}

enum class FerryDirection(val index: Int, val dx: Int, val dy: Int) {
  NORTH(0, 0, 1),
  EAST(1, 1, 0),
  SOUTH(2, 0, -1),
  WEST(3, -1, 0)
}