import java.io.File

fun main(args: Array<String>) {
  day8part1()
  day8part2()
}

fun day8part1() {
  var result = execute(day8input())
  println(result)
}

fun day8part2() {
  val instructions = day8input()
  var result: ExecutionResult

  var line = 0
  do {
    while (!arrayOf("nop", "jmp").contains(instructions[line].op)) {
      line++
    }

    instructions[line].op = if (instructions[line].op == "nop") "jmp" else "nop"
    result = execute(instructions)
    instructions[line].op = if (instructions[line].op == "nop") "jmp" else "nop"
    line++
  } while (!result.terminated)
  println(result)
}

fun execute(instructions: MutableList<Instruction>): ExecutionResult {
  var pc = 0
  var accumulator = 0
  val visited = mutableSetOf<Int>()

  while (!visited.contains(pc) && pc < instructions.size) {
    visited.add(pc)
    val instruction = instructions[pc]

    when (instruction.op) {
      "nop" -> pc++
      "acc" -> {
        accumulator += instruction.arg
        pc++
      }
      "jmp" -> pc += instruction.arg
    }
  }

  return ExecutionResult(pc >= instructions.size, accumulator)
}


fun day8input(): MutableList<Instruction> {
  val instructions = mutableListOf<Instruction>()
  File("input/day8.txt").forEachLine {
    val pieces = it.split(" ")
    instructions.add(Instruction(pieces[0], pieces[1].replace("+", "").toInt()))
  }

  return instructions
}

data class Instruction(var op: String, var arg: Int)
data class ExecutionResult(val terminated: Boolean, val accumulator: Int)