import java.io.File

fun main(args: Array<String>) {
  val products = mutableListOf<Product>()
  var ingredientsAllergenPossibility = mutableMapOf<String, MutableSet<String>>()

  File("input/day21.txt").forEachLine {
    if (it.isNotBlank()) {
      val pieces = it.trim().split(" (contains ")
      val product = Product(pieces[0].split(" ").toMutableSet(), pieces[1].replace(")", "").split(", ").toMutableSet())
      products.add(product)
    }
  }

  for (product in products) {
    for (ingredient in product.ingredients) {
      ingredientsAllergenPossibility.putIfAbsent(ingredient, mutableSetOf())
      ingredientsAllergenPossibility[ingredient]!!.addAll(product.allergens)
    }
  }

  for (ingredient in ingredientsAllergenPossibility.keys) {
    for (product in products) {
      if (!product.ingredients.contains(ingredient)) {
        var i = ingredientsAllergenPossibility[ingredient]!!.iterator()

        while (i.hasNext()) {
          if (product.allergens.contains(i.next())) {
            i.remove()
          }
        }
      }
    }
  }


  while (ingredientsAllergenPossibility.filter { it.value.size > 1 }.isNotEmpty()) {
    for (canon in ingredientsAllergenPossibility.filter { it.value.size == 1 }) {
      for (bad in ingredientsAllergenPossibility.filter { it.value.size > 1 }) {
        if (canon == bad) continue
        bad.value.remove(canon.value.first())
      }
    }
  }

  var count = 0
  ingredientsAllergenPossibility = ingredientsAllergenPossibility.filter { it.value.size > 0 }.toMutableMap()

  val answer = ingredientsAllergenPossibility.map { it.key }
  for (product in products) {
    count += product.ingredients.filter { !answer.contains(it) }.size
  }

  println(count)
  println(ingredientsAllergenPossibility.map { Allergen(it.key, it.value.first()) }.sortedBy { it.allergen }.map { it.ingredient }.joinToString(","))
}

data class Product(val ingredients: MutableSet<String>, val allergens: MutableSet<String>)
data class Allergen(val ingredient: String, val allergen: String)