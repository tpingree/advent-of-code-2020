import java.math.BigInteger

fun main(args: Array<String>) {
  val idToCup = mutableMapOf<Int, Cup>()

  var head = Cup(5)
  head.next = head
  head.prev = head
  idToCup[head.id] = head

  var current = head
  var ids = listOf(3, 8, 9, 1, 4, 7, 6, 2)

  for (i in ids) {
    val next = Cup(i)
    append(current, next)
    current = next
    idToCup[i] = next
  }

  for (i in 10..1000000) {
    val next = Cup(i)
    append(current, next)
    current = next
    idToCup[i] = next
  }

  // printCups(head)
  current = head

  // printCups(current)
  for (i in 1..10000000) {
    val cut = cut(current.next!!, 3)

    var insertId = current.id
    do {
      insertId--
      insertId = if (insertId < 1) 1000000 else insertId
    } while (contains(cut, insertId))

    append(idToCup[insertId]!!, cut)
    current = current.next!!
    // printCups(current)
  }

  // printCups(current)

  while (current.id != 1) {
    current = current.next!!
  }
  println(current.next!!.id.toBigInteger().multiply(current.next!!.next!!.id.toBigInteger()))
}

class Cup(val id: Int) {
  var next: Cup? = null
  var prev: Cup? = null
}

fun contains(cup: Cup, id: Int): Boolean {
  var current = cup

  while (true) {
    if (current.id == id) return true
    if (current.next != null) {
      current = current.next!!
    } else {
      return false
    }
  }
}

fun cut(n: Cup, amount: Int): Cup {
  val start = n.prev!!
  var end = n

  for (i in 1..amount) {
    end = end.next!!
  }

  n.prev = null
  end.prev!!.next = null

  start.next = end
  end.prev = start
  return n
}

fun append(start: Cup, tail: Cup) {
  var end = start.next!!
  start.next = tail
  tail.prev = start

  var tailEnd = tail
  while (tailEnd.next != null) {
    tailEnd = tailEnd.next!!
  }

  end.prev = tailEnd
  tailEnd.next = end
}

fun printCups(head: Cup) {
  var current = head

  do {
    print("${current.id} -> ")
    current = current.next!!
  } while (current != head)

  println()
}