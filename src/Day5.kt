import java.io.File
import kotlin.streams.toList

fun main(args: Array<String>) {
  day5part1()
  day5part2()
}

fun day5part1() {
  val seats = day5Input().map { it.toId() }.toList().sorted()
  println(seats[seats.size - 1])
}

fun day5part2() {
  val ids = day5Input().map { it.toId() }.toList().sorted()

  for (i in 32 until 913) {
    if (!ids.contains(i)) {
      println(i)
    }
  }
}

fun day5Input(): List<BoardingPass> {
  var boardingPasses = mutableListOf<BoardingPass>()


  File("input/day5.txt").forEachLine {
    var currentSize = 64
    var row = 0

    for (i in 0 until 7) {
      if (it[i] == 'B') {
        row += currentSize
      }

      currentSize /= 2
    }

    currentSize = 4
    var col = 0

    for (i in 7 until 10) {
      if (it[i] == 'R') {
        col += currentSize
      }

      currentSize /= 2
    }

    boardingPasses.add(BoardingPass(row, col))
  }

  return boardingPasses
}


data class BoardingPass(val row: Int, val col: Int) {
  fun toId() = (row * 8) + col
}