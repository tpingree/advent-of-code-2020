import java.io.File
import java.math.BigInteger

fun main(args: Array<String>) {
  day9part2(day9part1(25))
}

fun day9part1(preambleSize: Int): BigInteger {
  val input = day9input()
  var current = mutableSetOf<BigInteger>()

  for (i in 0 until preambleSize) {
    current.add(input[i])
  }

  var i = preambleSize
  var found = true
  var checking = BigInteger.ZERO
  while (found) {
    checking = input[i]
    val removing = input[i - preambleSize]

    for (j in i - 1 downTo i - preambleSize) {
      found = current.contains(checking.minus(input[j])) && (input[j] != checking.minus(input[j]))
      if (found) break
    }

    current.remove(removing)
    current.add(checking)
    i++
  }

  println(checking)
  return checking
}

fun day9part2(search: BigInteger) {
  val input = day9input()

  for (i in input.indices) {
    var j = i
    var start = search
    var end = BigInteger.ZERO
    var currentSum = BigInteger.ZERO

    while (search.minus(currentSum).compareTo(BigInteger.ZERO) == 1) {
      start = start.min(input[j])
      end = end.max(input[j])
      currentSum = currentSum.add(input[j++])
    }

    if (search.compareTo(currentSum) == 0) {
      println("$start + $end = ${start.add(end)}")
      return
    }
  }
}

fun day9input(): List<BigInteger> {
  val input = mutableListOf<BigInteger>()

  File("input/day9.txt").forEachLine {
    input.add(BigInteger(it.trim()))
  }

  return input
}