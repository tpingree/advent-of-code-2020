import java.io.File

fun main(args: Array<String>) {
  day6part1()
  day6part2()
}

fun day6part1() {
  println(day6input().map { party ->
    var letters = mutableSetOf<Char>()
    party.forEach {
      for (c in it) {
        letters.add(c)
      }
    }
    letters.size
  }.sum())
}

fun day6part2() {
  println(day6input().map { party ->
    var first = true
    var letters = mutableSetOf<Char>()

    party.forEach {
      if (first) {
        for (c in it) {
          letters.add(c)
        }
      } else {
        var iterator = letters.iterator()
        while (iterator.hasNext()) {
          if (!it.contains(iterator.next())) {
            iterator.remove()
          }
        }
      }

      first = false
    }

    letters.size
  }.sum())
}

fun day6input(): MutableList<MutableList<String>> {
  var parties = mutableListOf<MutableList<String>>()
  var party = mutableListOf<String>()

  File("input/day6.txt").forEachLine {
    var line = it.trim()

    if (it.isEmpty()) {
      parties.add(party)
      party = mutableListOf()
    } else {
      party.add(line)
    }
  }

  parties.add(party)
  return parties
}
