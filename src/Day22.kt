import java.io.File

fun main(args: Array<String>) {
  var rawHands = File("input/day22.txt").readText().trim().split("\n\n")
  val playerHand = rawHands[0].split("\n").toMutableList()
  playerHand.removeAt(0)
  val crabHand = rawHands[1].split("\n").toMutableList()
  crabHand.removeAt(0)

  var player = Player(playerHand.map { it.toInt() }.toMutableList())
  var crab = Player(crabHand.map { it.toInt() }.toMutableList())
  day22part1(player, crab)

  player = Player(playerHand.map { it.toInt() }.toMutableList())
  crab = Player(crabHand.map { it.toInt() }.toMutableList())
  day22part2(player, crab)
}

fun day22part1(player: Player, crab: Player) {
  while (!player.lost() && !crab.lost()) {
    val playerDraw = player.draw()
    val crabDraw = crab.draw()

    if (playerDraw > crabDraw) {
      player.deck.add(playerDraw)
      player.deck.add(crabDraw)
    } else {
      crab.deck.add(crabDraw)
      crab.deck.add(playerDraw)
    }
  }

  println(player.score().coerceAtLeast(crab.score()))
}

fun day22part2(player: Player, crab: Player) {
  recursiveCombat(player, crab)
  println(player.score().coerceAtLeast(crab.score()))
}

fun recursiveCombat(player: Player, crab: Player): Boolean {
  val previous = mutableSetOf<String>()

  while (!player.lost() && !crab.lost()) {
    val round = player.string() + "||" + crab.string()
    if (previous.contains(round)) return true
    previous.add(round)

    val playerDraw = player.draw()
    val crabDraw = crab.draw()

    var playerWins = if (playerDraw <= player.deck.size && crabDraw <= crab.deck.size) {
      val newPlayer = Player(ArrayList(player.deck).slice(0 until playerDraw).toMutableList())
      val newCrab = Player(ArrayList(crab.deck).slice(0 until crabDraw).toMutableList())
      recursiveCombat(newPlayer, newCrab)
    } else {
      playerDraw > crabDraw
    }

    if (playerWins) {
      player.deck.add(playerDraw)
      player.deck.add(crabDraw)
    } else {
      crab.deck.add(crabDraw)
      crab.deck.add(playerDraw)
    }
  }
  
  return crab.lost()
}

data class Player(val deck: MutableList<Int>) {
  fun lost() = deck.isEmpty()

  fun score(): Int {
    var reversed = deck.reversed()
    var score = 0
    for (i in reversed.indices) {
      score += reversed[i] * (i + 1)
    }
    return score
  }

  fun draw() = deck.removeAt(0)
  fun string() = deck.joinToString("-")
}