import java.io.File

fun main(args: Array<String>) {
  day2part1()
  day2part2()
}

fun day2part1() {
  println("Day 2 - Part 1")
  println("Valid passwords: " + day2Input().filter { it.part1Valid() }.count());
}

fun day2part2() {
  println("Day 2 - Part 2")
  println("Valid passwords: " + day2Input().filter { it.part2Valid() }.count());
}

fun day2Input(): List<Password> {
  var passwords = mutableListOf<Password>()

  File("input/day2.txt").forEachLine {
    val pieces = it.trim().split(": ", "-", " ")

    passwords.add(Password(
      pieces[0].toInt(),
      pieces[1].toInt(),
      pieces[2],
      pieces[3]
    ))
  }

  return passwords
}

class Password(
  val min: Int,
  val max: Int,
  val character: String,
  val value: String,
) {
  fun part1Valid() = value
    .replace(Regex("[^" + character + "]"), "")
    .length in min..max

  fun part2Valid() = (("" + value[min - 1]) == character) xor (("" + value[max - 1]) == character)
}