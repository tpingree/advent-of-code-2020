import java.io.File

fun main(args: Array<String>) {
  day1part1()
  day1part2()
}

fun day1part1() {
  println("Day 1 - Part 1")
  var values = day1Input();
  var valuesSet = values.toSet();

  values.forEach {
    if (valuesSet.contains(2020 - it)) {
      println("$it + ${2020 - it} = ${it * (2020 - it)}")
      return
    }
  }
}

fun day1part2() {
  println("Day 1 - Part 2")
  var values = day1Input();
  var valuesSet = values.toSet();

  for (i in 0 until values.size) {
    for (k in (i + 1) until values.size) {
      if (valuesSet.contains(2020 - (values[i] + values[k]))) {
        println("${values[i]} + ${values[k]} + ${2020 - (values[i] + values[k])} = ${values[i] * values[k] * (2020 - (values[i] + values[k]))}")
        return
      }
    }
  }
}

fun day1Input(): List<Int> {
  var values = mutableListOf<Int>()
  File("input/day1.txt").forEachLine {
    values.add(it.toInt())
  }
  return values
}