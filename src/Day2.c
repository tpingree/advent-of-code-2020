#include <stdio.h>
#include <string.h>
#include <time.h>
​
int validate(char line[150]) {
  int min = 0;
  int max = 0;
  char search;
  int count = 0;
​
  int i = 0;
  do {
    min = min * 10;
    min += line[i] - 48;
  } while(line[++i] != '-');
  i++;
​
  do {
    max = max * 10;
    max += line[i] - 48;
  } while(line[++i] != ' ');
  i++;
​
  search = line[i++];
  i++;
  i++;
​
  do {
    if (line[i] == search) {
      count++;
    }
  } while(line[i++] != '\n');
​
  return (count >= min && count <= max);
}
​
int main(int argc, char *argv[]) {
  clock_t start, end;
  double cpu_time_used;
​
  start = clock();
  FILE *passwords = NULL;
  char line[150];
  
  passwords = fopen("input.txt", "r");
​
  int count = 0;
  while(fgets(line, 150, passwords)) {
    count += validate(line);
  }
​
  end = clock();
  cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
​
  printf("%f\n", cpu_time_used); 
}