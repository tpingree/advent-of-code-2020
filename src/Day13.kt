import java.io.File
import java.math.BigInteger

fun main(args: Array<String>) {
  day13part2()
}

fun day13part1() {
  val pieces = File("input/day13.txt")
    .readText(Charsets.UTF_8)
    .trim()
    .split('\n')


  val arrival = pieces[0].trim().toInt()
  val departures = pieces[1]
    .trim()
    .split(',')
    .filter { it != "x" }
    .map { it.toInt() }

  departures.forEach {
    val closest = arrival / it
    val offset = arrival % it

    val nextPossible = (closest * it) + it
    val waitTime = nextPossible - arrival
    println("bus $it -> wait time = $waitTime, arrival = ${nextPossible + it}")
  }


  println("Arrival $arrival")
  println("Departures $departures")
}


fun day13part2brute() {
  val pieces = File("input/day13-test.txt")
    .readText(Charsets.UTF_8)
    .trim()
    .split('\n')

  val departures = pieces[1]
    .trim()
    .split(',')
    .map { if (it == "x") -1 else it.toInt() }

  var current = 0
  var i = 0
  var found = false
  while (!found) {
    current = departures[0] * i++
    // println("Checking $current")
    found = true

    for (j in 1 until departures.size) {
      if (departures[j] == -1) {
        continue
      }

      if (((current + j) % departures[j]) != 0) {
        found = false
        break
      }
    }
  }

  println(current)
}

fun day13part2() {
  val pieces = File("input/day13-m.txt").readText(Charsets.UTF_8).trim().split('\n')
  val raw = pieces[1].trim().split(',').map { if (it == "x") -1 else it.toInt() }

  var departures = mutableListOf<Departure>()
  for (i in raw.indices) {
    if (raw[i] != -1) {
      departures.add(Departure(raw[i].toBigInteger(), i.toBigInteger()))
    }
  }

  println(departures)

  var current = BigInteger.ZERO
  var increment = BigInteger.ZERO.add(departures.removeAt(0).id)
  var currentDeparture = departures[0]

  while (departures.isNotEmpty()) {
    if (((current + currentDeparture.offset).mod(currentDeparture.id)).equals(BigInteger.ZERO)) {
      println("Checked that ($current + ${currentDeparture.offset}) / ${currentDeparture.id} == 0")
      increment = increment.multiply(currentDeparture.id)
      departures.removeAt(0)

      if (departures.isEmpty()) {
        break
      }

      currentDeparture = departures[0]
      println("Matched on $current -- new increment $increment")
    } else {
      current = current.add(increment)
    }
  }
}

data class Departure(val id: BigInteger, val offset: BigInteger)
