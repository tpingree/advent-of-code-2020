import java.io.File
import java.math.BigInteger
import java.util.*
import kotlin.collections.ArrayList

fun main(args: Array<String>) {
  day14part2()
}

fun day14part1() {
  val memory = mutableMapOf<Int, BigBinary>()
  var mask = BigMask("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")

  File("input/day14.txt").forEachLine {
    if (it.startsWith("mask")) {
      mask = BigMask(it.replace("mask = ", "").trim())
    }

    if (it.startsWith("mem")) {
      var pieces = it.replace("mem[", "").trim().split("] = ")
      val binary = BigBinary.fromString(pieces[1])
      memory[pieces[0].toInt()] = mask.apply(binary)
    }
  }

  val sum = memory.values.map { it.toInteger() }.fold(BigInteger.ZERO, BigInteger::add)
  println(sum)
}

fun day14part2() {
  val memory = mutableMapOf<BigBinary, BigBinary>()
  var mask = BigMask("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")

  File("input/day14.txt").forEachLine {
    if (it.startsWith("mask")) {
      mask = BigMask(it.replace("mask = ", "").trim())
    }

    if (it.startsWith("mem")) {
      var pieces = it.replace("mem[", "").trim().split("] = ")

      val baseAddress = BigBinary.fromString(pieces[0])
      val addresses = mask.applyToMemoryAddress(baseAddress)
      for (address in addresses) {
        memory[address] = BigBinary.fromString(pieces[1])
      }
    }
  }

  val sum = memory.values.map { it.toInteger() }.fold(BigInteger.ZERO, BigInteger::add)
  println(sum)
}

data class BigBinary(var bits: BooleanArray) {
  companion object {
    fun fromString(raw: String): BigBinary {
      val bits = BooleanArray(36)
      var value = raw.toInt()

      for (i in 0 until 36) {
        bits[i] = value % 2 == 1
        value /= 2
      }

      return BigBinary(bits)
    }
  }

  fun toInteger(): BigInteger {
    var value = BigInteger.ZERO

    for (i in bits.indices) {
      if (bits[i]) {
        value = value.add(BigInteger.TWO.pow(i))
      }
    }
    return value
  }

  override fun toString(): String {
    val builder = StringBuilder()

    for (i in (bits.size - 1) downTo 0) {
      builder.append(if (bits[i]) "1" else "0")
    }

    return builder.toString()
  }

  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    if (other?.javaClass != javaClass) return false
    other as BigBinary
    if (!Arrays.equals(bits, other.bits)) return false
    return true
  }

  override fun hashCode(): Int {
    return Arrays.hashCode(bits)
  }
}

class BigMask {
  var masks = mutableListOf<BigMaskValue>()

  constructor(raw: String) {
    masks = raw.toCharArray().map {
      when (it) {
        '1' -> BigMaskValue.ONE
        '0' -> BigMaskValue.ZERO
        else -> BigMaskValue.X
      }
    }.asReversed().toMutableList()
  }

  constructor(masks: MutableList<BigMaskValue>) {
    this.masks = masks
  }

  fun toBigBinary(): BigBinary {
    return BigBinary(masks.map { it == BigMaskValue.ONE }.toBooleanArray())
  }

  fun cloneWithReplace(): List<BigMask> {
    for (i in masks.indices) {
      if (masks[i] == BigMaskValue.X) {
        val newMasks = listOf(
          BigMask(ArrayList(masks)),
          BigMask(ArrayList(masks))
        )

        newMasks[0].masks[i] = BigMaskValue.ZERO
        newMasks[1].masks[i] = BigMaskValue.ONE
        return newMasks
      }
    }

    return listOf()
  }

  fun hasX(): Boolean = masks.contains(BigMaskValue.X)


  fun expandX(): List<BigBinary> {
    var addresses = mutableListOf(this)
    while (addresses[0].hasX()) {
      var next = mutableListOf<BigMask>()
      for (address in addresses) {
        next.addAll(address.cloneWithReplace())
      }
      addresses = next
    }

    return addresses.map { it.toBigBinary() }
  }

  fun applyToMemoryAddress(baseAddress: BigBinary): List<BigBinary> {
    val copy = BigMask(ArrayList(masks))
    copy.mask(baseAddress)
    return copy.expandX()
  }


  fun apply(binary: BigBinary): BigBinary {
    for (i in masks.indices) {
      binary.bits[i] = masks[i].apply(binary.bits[i])
    }

    return binary
  }


  fun mask(mask: BigBinary) {
    for (i in masks.indices) {
      if (mask.bits[i] && masks[i] == BigMaskValue.ZERO) masks[i] = BigMaskValue.ONE
    }
  }
}

enum class BigMaskValue {
  ONE {
    override fun apply(value: Boolean) = true
  },
  ZERO {
    override fun apply(value: Boolean) = false
  },
  X {
    override fun apply(value: Boolean) = value
  };

  abstract fun apply(value: Boolean): Boolean
}