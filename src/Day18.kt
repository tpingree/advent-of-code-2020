import java.io.File
import java.math.BigInteger

fun main(args: Array<String>) {
  println(day18input().map { evaluateParens(it) }.fold(BigInteger.ZERO, BigInteger::add))
  println(day18input().map { evaluateParensWithPrecedence(it) }.fold(BigInteger.ZERO, BigInteger::add))
}

fun day18input(): MutableList<String> {
  val expressions = mutableListOf<String>()
  File("input/day18.txt").forEachLine {
    expressions.add(it.trim())
  }
  return expressions
}

fun evaluateParens(expression: String): BigInteger {
  var current = expression

  while (current.contains('(')) {
    current = current.replace(Regex("""\(([0-9 +*]+)\)""")) {
      "" + evaluateWithoutParens(it.groupValues[1])
    }
  }

  return evaluateWithoutParens(current)
}

fun evaluateWithoutParens(expression: String): BigInteger {
  val pieces = expression.split(" ")
  var current = BigInteger.ZERO
  var operator = ""

  for (i in pieces.indices) {
    when (pieces[i]) {
      "+", "*" -> operator = pieces[i]
      else -> {
        when (operator) {
          "+" -> current = current.add(pieces[i].toBigInteger())
          "*" -> current = current.multiply(pieces[i].toBigInteger())
          else -> current = pieces[i].toBigInteger()
        }
      }
    }
  }

  return current
}


// Evaluate addition operations first
fun evaluateParensWithPrecedence(expression: String): BigInteger {
  var current = expression

  while (current.contains('(')) {
    current = current.replace(Regex("""\(([0-9 +*]+)\)""")) {
      "" + evaluateWithoutParensWithPrecedence(it.groupValues[1])
    }
  }

  return evaluateWithoutParensWithPrecedence(current)
}


fun evaluateWithoutParensWithPrecedence(expression: String): BigInteger {
  return expression.split(" * ").map {
    it.split(" + ").map { it.toBigInteger() }.fold(BigInteger.ZERO, BigInteger::add)
  }.fold(BigInteger.ONE, BigInteger::multiply)
}